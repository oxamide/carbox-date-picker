import generate from './generate';
import { expose_picker_on_input_focus, hide_picker_on_click_away } from './utils/event-listeners';

class Carbox_picker extends HTMLElement {
    constructor() {
        super();
        this.input = document.createElement("input");
        this.input.type = "text";

        this.attachShadow({mode: 'open'});

        // clear all inherited styling
        const style = document.createElement("style");
        style.textContent = ":host { all: initial }"
        this.shadowRoot.appendChild(style);
    }

    connectedCallback() {
        this.shadowRoot.appendChild(this.input);
        this.container = generate(this.input);
        
        expose_picker_on_input_focus(this.input, this.container);
        hide_picker_on_click_away(this.input, this.container);
    }
}

customElements.define("carbox-picker", Carbox_picker)

export const expose_picker_on_input_focus = (input, picker) => {
  input.addEventListener("focus", () => {
    picker.style.display = "block";
  });
};

export const hide_picker_on_click_away = (input, picker) => {
  // hide picker when a click occurs anywhere in window
  window.addEventListener("click", () => {
    picker.style.display = "none";
  });

  // exclude input from the above listener
  input.addEventListener("click", e => {
    e.stopPropagation();
  });
};
